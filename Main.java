public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.black, new Point(), 5);
        Triangle triangle = new Triangle(Color.red, new Point(0, 0), new Point(1, 0), new Point(0, 1));
        Square square = new Square(Color.white, new Point(1, 1), 5);

        Shape shape = triangle;
        Object o = triangle;
        Triangle triangle1 = (Triangle) o;

        Shape[] shapes = {circle, triangle, square};
        printArrayElements(shapes);

        Shape maxShape = maxShapeArea(shapes);
        System.out.println("Фигура с наиболььшей площадью " + maxShape);
    }

    private static void printArrayElements(Object[] objects) {
        for (Object object : objects) {
            System.out.println(object);
        }
    }

    private static Shape maxShapeArea(Shape[] shapes) {
        Shape maxShape = null;
        double maxArea = Double.NEGATIVE_INFINITY;
        for (Shape shape : shapes) {
            double area = shape.area();
            if (area > maxArea) {
                maxArea = area;
                maxShape = shape;
            }
        }
        return maxShape;
    }
}
